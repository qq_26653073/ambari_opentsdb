#!/usr/bin/env python
#coding=utf-8
from resource_management import *

# config object that holds the configurations declared in the config xml file

# config object that holds the configurations declared in the opentsdb-config xml file
config = Script.get_config()
tmp_dir = Script.get_tmp_dir()
stack_root = Script.get_stack_root()

# store the log file for the service commands outputs
tsd_network_port = config['configurations']['opentsdb-config']['tsd_network_port']
tsd_storage_hbase_zk_quorum = config['configurations']['opentsdb-config']['tsd_storage_hbase_zk_quorum']
tsd_storage_hbase_zk_basedir = config['configurations']['opentsdb-config']['tsd_storage_hbase_zk_basedir']

# opentsdb need create hbase tables ,use the script in opentsdb to finish this job.
opentsdb_table_create = config["configurations"]["opentsdb-env"]["create_table_script"]

#when use the script, we should assign the hbase dir.
opentsdb_config_file_dir = config["configurations"]["opentsdb-env"]["opentsdb.config_file_dir"]

# path of log file
opentsdb_log = config["configurations"]["opentsdb-env"]["opentsdb.log"]

# hbase configuration dir
hbase_dir = config["configurations"]["opentsdb-env"]["opentsdb.log"]

# the path of start script
tsdb = config["configurations"]["opentsdb-env"]["tsdb"]

#
opentsdb_user = config["configurations"]["opentsdb-env"]["opentsdb_user"]

#
opentsdb = config["configurations"]["opentsdb-env"]["opentsdb"]

# get the java path ,it is used in the script of starting opentsdb.
java_home = config["hostLevelParams"]["java_home"]
java_path = format("{java_home}/bin")