#coding=utf-8
import sys, os, pwd, signal, time
from resource_management import *
from subprocess import call
from resource_management.core.logger import Logger
from resource_management.libraries.functions import format
from resource_management.core.resources.system import Execute, File
from resource_management.libraries.functions.check_process_status import check_process_status
from resource_management.core import shell


class Master(Script):
    def install(self, env):
        import params
        import status_params
        env.set_params(params)
        env.set_params(status_params)
        Logger.info("The dependencies is installing!")
        self.install_packages(env)
        # create hbase table which opentsdb need.
        Execute("su hbase -l -c 'env COMPRESSION=NONE HBASE_HOME=/usr/hdp/current/hbase-client /usr/share/opentsdb/tools/create_table.sh'")
        pass

    def configure(self, env):
        import params
        import opentsdb
        env.set_params(params)
        Logger.info("change the configruation.")
        opentsdb.opentsdb_conf()

    def stop(self, env):
        import params
        env.set_params(params)
        Execute("service opentsdb stop")

    def status(self, env):
        import status_params
        env.set_params(status_params)
        print "opentsdb pid : " + status_params.opentsdb_pid
        check_process_status(status_params.opentsdb_pid)

    def start(self, env):
        import params
        import status_params
        env.set_params(params)
        env.set_params(status_params)
        self.configure(env)
        Logger.info("change the configruation.")
        if not os.path.exists(status_params.opentsdb_pid_dir):
            os.makedirs(status_params.opentsdb_pid_dir)
        Execute("service opentsdb start")

if __name__ == "__main__":
    Master().execute()
