#!/usr/bin/env python
#coding=utf-8

from resource_management import *
import sys

def opentsdb_conf():
    import params

    # directories = [status_params.opentsdb_log, status_params.opentsdb_pid_dir, status_params.opentsdb_config_file_dir]

    Directory(params.opentsdb_config_file_dir,
              create_parents=True
              )

    File(format("{opentsdb_config_file_dir}/opentsdb.conf"),
         owner='hbase',
         content=InlineTemplate(params.opentsdb),
         )

    File(format("{tsdb}"),
         owner='hbase',
         content=Template("tsdb.j2")
         )



    # File(format("{opentsdb_config_file_dir}/opentsdb.conf"),
    #      content=Template("opentsdb.conf.j2"),
    #      configurations=configurations)
