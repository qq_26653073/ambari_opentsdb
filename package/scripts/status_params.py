#!/usr/bin/env python
#coding=utf-8
from resource_management import *
import sys, os

config = Script.get_config()

# pid config, opentsdb_pid="/var/run/opentsdb/opentsdb.pid"
opentsdb_pid_dir = config['configurations']['opentsdb-env']['opentsdb_pid_dir']
opentsdb_pid = format("{opentsdb_pid_dir}/opentsdb.pid")
